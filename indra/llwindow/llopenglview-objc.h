//
//  LLOpenGLView.h
//  SecondLife
//
//  Created by Geenz on 10/2/12.
//
//

#import <Cocoa/Cocoa.h>
#import <IOKit/IOKitLib.h>
#import <CoreFoundation/CFBase.h>
#import <CoreFoundation/CFNumber.h>
#include <string>
#include "llwindowmacosx-objc.h"

@interface LLOpenGLView : NSOpenGLView <NSTextInputClient>
{
	std::string mLastDraggedUrl;
	unsigned int mModifiers;
	float mMousePos[2];
	bool mHasMarkedText;
	unsigned int mMarkedTextLength;
	NSAttributedString *mMarkedText;
}
- (id) initWithSamples:(NSUInteger)samples;
- (id) initWithSamples:(NSUInteger)samples andVsync:(BOOL)vsync;
- (id) initWithFrame:(NSRect)frame withSamples:(NSUInteger)samples andVsync:(BOOL)vsync;

- (void)commitCurrentPreedit;

// rebuildContext
// Destroys and recreates a context with the view's internal format set via setPixelFormat;
// Use this in event of needing to rebuild a context for whatever reason, without needing to assign a new pixel format.
- (BOOL) rebuildContext;

// rebuildContextWithFormat
// Destroys and recreates a context with the specified pixel format.
- (BOOL) rebuildContextWithFormat:(NSOpenGLPixelFormat *)format;

// These are mostly just for C++ <-> Obj-C interop.  We can manipulate the CGLContext from C++ without reprecussions.
- (CGLContextObj) getCGLContextObj;
- (CGLPixelFormatObj*)getCGLPixelFormatObj;

- (unsigned long) getVramSize;

// <exodus>
// Helper functions for running on 10.6.
- (NSRect)exodusConvertRectToBacking:(NSRect)rect;
- (NSSize)exodusConvertSizeToBacking:(NSSize)size;
- (NSPoint)exodusConvertPointToBacking:(NSPoint)point;
// </exodus>

@end

// <exodus>
#if __MAC_OS_X_VERSION_MAX_ALLOWED <= 1060
@interface LLOpenGLView (Retina)
// This allows us to compile 10.7+ features using the 10.6 SDK.
- (NSRect)convertRectToBacking:(NSRect)rect;
- (NSSize)convertSizeToBacking:(NSSize)size;
- (NSPoint)convertPointToBacking:(NSPoint)point;
- (void)setWantsBestResolutionOpenGLSurface:(BOOL)maybe;
@end
#endif
// </exodus>

@interface LLNonInlineTextView : NSTextView
{
	LLOpenGLView *glview;
}

- (void) setGLView:(LLOpenGLView*)view;

@end

@interface LLNSWindow : NSWindow

- (NSPoint)convertToScreenFromLocalPoint:(NSPoint)point relativeToView:(NSView *)view;
- (NSPoint)flipPoint:(NSPoint)aPoint;

@end

// <exodus>
#if __MAC_OS_X_VERSION_MAX_ALLOWED <= 1060
@interface LLNSWindow (Retina)
- (NSRect)convertRectFromScreen:(NSRect)aRect;
- (NSRect)convertRectToScreen:(NSRect)aRect;
@end
#endif
// </exodus>

@interface NSScreen (PointConversion)

/*
 Returns the screen where the mouse resides
 */
+ (NSScreen *)currentScreenForMouseLocation;

/*
 Allows you to convert a point from global coordinates to the current screen coordinates.
 */
- (NSPoint)convertPointToScreenCoordinates:(NSPoint)aPoint;

/*
 Allows to flip the point coordinates, so y is 0 at the top instead of the bottom. x remains the same
 */
- (NSPoint)flipPoint:(NSPoint)aPoint;

@end
