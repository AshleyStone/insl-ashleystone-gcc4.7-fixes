/**
 * @file exopastebin.h
 * @brief Helpers for posting to pastebin-like services.
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include <boost/bind.hpp>

class exoPastebin
{
public:
	typedef boost::function<void(const LLWString& text)> completion_function;
	static void generateLink(const std::string& content, const completion_function& callback);
	static bool hasPasteData();
	static void doPaste(const completion_function& callback);
	static void copyLinkToClipboard(const LLWString& text, S32 start, S32 len);
private:
	// Callback for copyLinkToClipboard.
	static void putGeneratedLinkOnClipboard(const LLWString& text);
};
