/** 
 * @file evonmanager.cpp
 * @brief System used for downloading client detection database and other bits.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "evonmanager.h"

#include "llsdserialize.h"

#define EVON_LABEL "EVON"
#define EVON_VERSION "2.0"
#define EVON_VERSION_NUM 2.0

static S32 fromLegacyEvon(std::istream& str, LLSD& sd);

S32 evonManager::toEVON(const LLSD& sd, std::ostream& str, std::string type, std::string version)
{
	str << "# " << EVON_LABEL << " " << EVON_VERSION << " " << type << " " << version << "\n";

	return LLSDSerialize::toNotation(sd, str);
}

S32 evonManager::fromEVON(LLSD& sd, std::istream& str, std::string type, double version)
{
	std::string versionData;
	std::getline(str, versionData);

	if (!versionData.empty())
	{
		std::vector<std::string> versionTokens;
		LLStringUtil::getTokens(versionData, versionTokens, " ");

		if (versionTokens[0] == "#")
		{
			if (versionTokens[1] == EVON_LABEL)
			{
				if (atof(versionTokens[2].c_str()) <= EVON_VERSION_NUM)
				{
					if (versionTokens[3] == type)
					{
						if (atof(versionTokens[4].c_str()) <= version)
						{
							if(versionTokens[2] == "1.0")
							{
								return fromLegacyEvon(str, sd);
							}
							else
							{
								return LLSDSerialize::fromNotation(sd, str, LLSDSerialize::SIZE_UNLIMITED);
							}
						}
						else llinfos << "Import version is \"" << versionTokens[4] << "\" not \"" << version << "\", failed to import." << llendl;
					}
					else llinfos << "Import type is \"" << versionTokens[3] << "\" not \"" << type << "\", failed to import." << llendl;
				}
				else llinfos << "File version is \"" << versionTokens[2] << "\" not \"" << EVON_VERSION << "\", failed to import." << llendl;
			}
			else llinfos << "File type is \"" << versionTokens[1] << "\" not \"" << EVON_LABEL << "\", failed to import." << llendl;
		}
		else llinfos << "File type is incorrect, did not detect \"#\" on first line, failed to import." << llendl;
	}
	else llinfos << "Unable to get first line, failed to import." << llendl;

	return 0;
}

// TODO: Remove this and drop support for v1.
// This actually transforms EVONv1 to XML without any understanding of
// what it's working with. It shouldn't crash, but the output may be absurd.
// static
S32 fromLegacyEvon(std::istream& str, LLSD& sd)
{
	std::stringstream xml;
	std::vector<std::string> tags;
	bool in_value = false;
	while(str.good()) {
		char token = str.get();
		if(token == '{') {
			in_value = false;
			continue;
		}
		if(token == '}') {
			if(!tags.empty()) {
				xml << "</" << tags.back() << ">";
				tags.pop_back();
			}
			in_value = false;
		}
		if(token == '"') {
			bool is_escaped = false;
			std::ostringstream value;
			while(str.good()) {
				char c;
				str.get(c);
				if(c == '\\') {
					if(is_escaped)
						value << c;
					is_escaped = !is_escaped;
					continue;
				}
				if(c == '"') {
					if(is_escaped) {
						value << c;
						is_escaped = false;
						continue;
					} else {
						break;
					}
				}
				value << c;
			}
			if(!in_value) {
				xml << "<" << value.str() << ">";
				tags.push_back(value.str());
			} else {
				xml << LLSDXMLFormatter::escapeString(value.str());
				if(!tags.empty()) {
					xml << "</" << tags.back() << ">";
					tags.pop_back();
				}
				in_value = false;
			}
		}
		if(token == ':') {
			in_value = true;
		}
	}
	
	return LLSDSerialize::fromXML(sd, xml);
}
