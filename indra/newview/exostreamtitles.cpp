/**
 * @file exostreamtitles.cpp
 * @brief Displays titles for the current stream as they change.
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "llaudioengine.h"
#include "llevents.h"
#include "llmainlooprepeater.h"
#include "llnotificationsutil.h"
#include "llstreamingaudio.h"

#include "exostreamtitles.h"

exoStreamTitles::exoStreamTitles()
{
	// Bail out if we don't have a metadata signal.
	if(!gAudiop || !gAudiop->getStreamingAudioImpl()->getMetadataSignal())
	{
		return;
	}
	LLEventPumps::instance().obtain("exostreamtitles").listen("title", boost::bind(&exoStreamTitles::onPump, this, _1));
	gAudiop->getStreamingAudioImpl()->getMetadataSignal()->connect(boost::bind(&exoStreamTitles::onStreamMetadata, this, _1, _2));
}

//static
void exoStreamTitles::initClass()
{
	// Ensuring that an instance of ourselves exists is sufficient.
	exoStreamTitles::instance();
}

void exoStreamTitles::onStreamMetadata(const std::string& artist, const std::string& title)
{
	LLSD map;
	LLSD metadata;
	metadata["title"] = title;
	metadata["artist"] = artist;
	map["pump"] = "exostreamtitles";
	map["payload"] = metadata;
	LLEventPumps::instance().obtain("mainlooprepeater").post(map);
}

bool exoStreamTitles::onPump(const LLSD& data)
{
	LLSD args;
	args["TITLE"] = data["title"];
	if(!data["artist"].asString().empty())
	{
		args["ARTIST"] = data["artist"];
		LLNotificationsUtil::add("ExodusStreamMetadata", args);
	}
	else
	{
		LLNotificationsUtil::add("ExodusStreamMetadataNoArtist", args);
	}
	return false;
}
