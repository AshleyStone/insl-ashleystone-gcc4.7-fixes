/** 
 * @file exofloaterstatistics.h
 * @brief EXOFloaterStatistics class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_STATISTICS
#define EXO_FLOATER_STATISTICS

#include "llfloater.h"

class LLTextBox;

class exoFloaterStatistics : public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterStatistics(const LLSD& key);

	BOOL postBuild();

	void draw();
	
	LLTextBox* mText;
	
	BOOL onDoubleClick(LLUICtrl* ctrl, S32 x, S32 y, MASK mask);

	void textAlignLeft();
	void textAlignCenter();
	void textAlignRight();

	LLRect windowSize;

	bool renderBackground;
};

#endif // EXO_FLOATER_STATISTICS
