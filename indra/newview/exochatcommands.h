/** 
 * @file exochatcommands.h
 * @brief exoChatCommands class definitions
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_CHAT_COMMANDS
#define EXO_CHAT_COMMANDS

class exoChatCommands
{
private:
	exoChatCommands(){}

public:
	static bool phraseChat(const std::string& data);

private:
	static void systemTip(const std::string& data);
	static void systemMessage(const std::string& data);
};

#endif // EXO_CHAT_COMMANDS
